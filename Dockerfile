FROM registry.opensuse.org/devel/openqa/containers15.2/openqa_webui:latest

RUN zypper --no-gpg-checks ref && zypper --no-gpg-checks update -y && zypper --no-gpg-checks in -y 'perl(Mojolicious::Plugin::OAuth2)'

RUN usermod -d /var/lib/openqa -s /bin/bash geekotest

COPY files /root/files
RUN cp /root/files/gnomeos.png /usr/share/openqa/assets/images/gnomeos.png && \
  mkdir -p /usr/share/openqa/templates/webapi/branding/gnomeOS && \
  cp /root/files/*html* /usr/share/openqa/templates/webapi/branding/gnomeOS/. && \
  rm -rf /root/files && chown geekotest /usr/share/openqa/assets/images/gnomeos.png && \
  echo -e "\n! gnomeos.png\n< images/gnomeos.png" >> /usr/share/openqa/assets/assetpack.def

RUN rm -f /etc/apache2/vhosts.d/openqa-ssl.conf
